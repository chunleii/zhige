package com.oly.web.mould.enums;

public enum BlogOperatorType {
    /**
     * 其它
     */
    OTHER,
    /**
     * 用户
     */
    USER,
}
