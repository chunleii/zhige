package com.oly.web.mould;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.oly.common.model.entity.base.BaseTheme;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BlogTheme extends BaseTheme {

    private static final long serialVersionUID = 1L;

}