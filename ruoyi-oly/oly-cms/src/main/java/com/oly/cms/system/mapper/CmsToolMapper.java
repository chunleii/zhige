package com.oly.cms.system.mapper;

import java.util.List;

public interface CmsToolMapper {

    public List<String> listCatIds();

    public List<String> listTagIds();

    public List<String> listPostIds();
    
}
