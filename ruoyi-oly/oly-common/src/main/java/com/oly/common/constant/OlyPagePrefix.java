package com.oly.common.constant;

/**
 * 请求前缀常量
 */
public class OlyPagePrefix {
    public static final String indexPage="/blog";
    public static final String postsPage="/blog/post";
    public static final String postPage="/blog/post/";
    public static final String catsPage="/blog/cat";
    public static final String catPage="/blog/cat/";
    public static final String tagsPage="/blog/tag";
    public static final String tagPage="/blog/tag/";
    public static final String aboutPage="/blog/about";
    public static final String linksPage="/blog/links";   
}
